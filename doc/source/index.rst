
Welcome to the fontinfo documentation!
===============================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   fontinfo

Indices and tables
-------------------------------------------------------------------------------

* :ref:`genindex`
* :ref:`modindex`
