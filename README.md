# fontinfo

Represent information about a font in a format agnostic fashion.

## Installation

```shell
pip install fontinfo
```
